﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ResourceBank : MonoBehaviour
{
    public static Action<ResType> OnResChanged;

    Dictionary<ResType, int> numberNames = new Dictionary<ResType, int>();
   public enum ResType
    {
        Рабочий,
        Дерево,
        Дом,
        Деньги
    }

    

    

    public static ResourceBank Instance;
    void Start()
    {
        numberNames.Add(ResType.Рабочий, 2);
        numberNames.Add(ResType.Дерево, 0);
        numberNames.Add(ResType.Дом, 0);
        numberNames.Add(ResType.Деньги, 0);


        StartCoroutine(OnChangedWood());


    }

   

    public void ChangeResource(ResType res, int val)
    {
        numberNames[res] += val;
        OnResChanged(res);
    }
    public int GetResource(ResType res)
    {
        return numberNames[res];
    }

    IEnumerator OnChangedWood()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            ChangeResource(ResType.Дерево, GetResource(ResType.Рабочий));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
