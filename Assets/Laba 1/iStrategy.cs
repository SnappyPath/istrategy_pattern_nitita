﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iStrategy
{
    void Perform(Transform transform);
    
}
