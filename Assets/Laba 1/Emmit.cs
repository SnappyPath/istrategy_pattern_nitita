﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emmit : iStrategy
{
    public int amountEmit;
    public Emmit(int integer)
    {
        amountEmit = integer;
    }
    public void Perform(Transform transform)
    {
        transform.GetComponent<ParticleSystem>().Emit(amountEmit);
    }
}
